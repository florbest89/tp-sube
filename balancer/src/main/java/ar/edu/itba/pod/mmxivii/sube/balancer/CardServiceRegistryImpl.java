package ar.edu.itba.pod.mmxivii.sube.balancer;

import ar.edu.itba.pod.mmxivii.sube.common.CardService;
import ar.edu.itba.pod.mmxivii.sube.common.CardServiceRegistry;

import javax.annotation.Nonnull;

import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class CardServiceRegistryImpl extends UnicastRemoteObject implements CardServiceRegistry
{
	private static final long serialVersionUID = 2473638728674152366L;
	private final List<CardService> serviceList = Collections.synchronizedList(new ArrayList<CardService>());

	protected CardServiceRegistryImpl() throws RemoteException {}

	@Override
	public void registerService(@Nonnull CardService service) throws RemoteException
	{
		serviceList.add(service);
	}

	@Override
	public void unRegisterService(@Nonnull CardService service) throws RemoteException
	{
		serviceList.remove(service);
	}

	@Override
	public Collection<CardService> getServices() throws RemoteException
	{
		return serviceList;
	}
	
	/**
	 * 
	 * @return un CardService al azar
	 * @throws RemoteException
	 */
	private CardService randomService(){
		if(serviceList.size() == 0){
			return null;
		}
		Random r = new Random();
		int rand_service = r.nextInt(serviceList.size());
		int i = 0;
		CardService resp = null;
		for (Iterator<CardService> iterator = serviceList.iterator(); iterator.hasNext();) {
			if(rand_service == i){
				resp = iterator.next();
				break;
			}
			i++;
		}
		System.out.println("toca:" + i);
		return resp;
	}
	
	private AtomicInteger next = new AtomicInteger(0);
	private CardService roundRobinService(){
		CardService resp = null;
		int serviceNumber = next.get();
		int i = 0;
		serviceNumber = serviceNumber % serviceList.size();
		next.set(serviceNumber);
		for (Iterator<CardService> iterator = serviceList.iterator(); iterator.hasNext();) {
			if(serviceNumber == i){
				resp = iterator.next();
				break;
			}
			i++;
		}
		System.out.println(resp);
		return resp;
	}
	
	/**
	 * 
	 * @return un CardService que le corresponde al UID
	 * @throws RemoteException
	 */
	private CardService uidedService(UID id){
		if(serviceList.size() == 0){
			return null;
		}
		int hash = id.hashCode();
		int number = hash % serviceList.size();
		System.out.println("toca: " + number);
		return serviceList.get(number);
	}
	
	//for tests proposes
	CardService getCardService(int i)
	{
		return serviceList.get(i);
	}

	CardService getCardService( UID id )
	{
		return uidedService(id);//randomService();// serviceList.get(0);
	}
}
