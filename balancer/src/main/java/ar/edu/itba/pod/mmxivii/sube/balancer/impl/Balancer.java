package ar.edu.itba.pod.mmxivii.sube.balancer.impl;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_CLIENT_BIND;
import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_REGISTRY_BIND;
import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_SERVICE_REGISTRY_BIND;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import javax.annotation.Nonnull;

import ar.edu.itba.pod.mmxivii.sube.common.BaseMain;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;
@Deprecated
public class Balancer extends BaseMain{
	
	
	private static Balancer main = null;

	private Balancer(@Nonnull String[] args) throws RemoteException, NotBoundException
	{
		super(args, DEFAULT_CLIENT_OPTIONS);
		getRegistry();
		setDelay();
		//aca expongo la interfaz para que se puedan registrar
		final EnhancedCardServiceRegistryImpl cardServiceRegistry = new EnhancedCardServiceRegistryImpl();
		bindObject(CARD_SERVICE_REGISTRY_BIND, cardServiceRegistry);

		
		//busco el server y expongo la interfaz para los clientes
		final CardRegistry cardRegistry = Utils.lookupObject(CARD_REGISTRY_BIND);
		final EnhancedCardClientImpl cardClient = new EnhancedCardClientImpl(cardRegistry, cardServiceRegistry);
		bindObject(CARD_CLIENT_BIND, cardClient);
	}

	public static void main(String[] args) throws Exception {
		main = new Balancer(args);
		main.run();
	}
		

	private void run()
	{
		System.out.println("Starting Balancer!");
		final Scanner scan = new Scanner(System.in);
		String line;
		do {
			line = scan.next();
			System.out.println("Balancer running");
		} while(!"x".equals(line));
		shutdown();
		System.out.println("Balancer exit.");
		System.exit(0);

	}

	public static void shutdown()
	{
		main.unbindObject(CARD_SERVICE_REGISTRY_BIND);
		main.unbindObject(CARD_CLIENT_BIND);
	}
}
