package ar.edu.itba.pod.mmxivii.sube.balancer.impl;

import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import ar.edu.itba.pod.mmxivii.sube.balancer.CardServiceRegistryImpl;
import ar.edu.itba.pod.mmxivii.sube.common.CardService;

/**
 * Esto es una implentación del CardServiceRegistry
 * Es donde se registran los nodos para poder ser balanceados
 * @author gmaldo
 *
 */
@Deprecated
public class EnhancedCardServiceRegistryImpl extends CardServiceRegistryImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected EnhancedCardServiceRegistryImpl() throws RemoteException {
		super();
	}
	/**
	 * 
	 * @return un CardService al azar
	 * @throws RemoteException
	 */
	private CardService randomService() throws RemoteException{
		Random r = new Random();
		int rand_service = r.nextInt(getServices().size());
		int i = 0;
		CardService resp = null;
		for (Iterator<CardService> iterator = getServices().iterator(); iterator.hasNext();) {
			if(rand_service == i){
				resp = iterator.next();
				break;
			}
			i++;
		}
		return resp;
	}
	
	private AtomicInteger next = new AtomicInteger(0);
	private CardService roundRobinService() throws RemoteException{
		CardService resp = null;
		int serviceNumber = next.get();
		int i = 0;
		serviceNumber = serviceNumber % getServices().size();
		next.set(serviceNumber);
		for (Iterator<CardService> iterator = getServices().iterator(); iterator.hasNext();) {
			if(serviceNumber == i){
				resp = iterator.next();
				break;
			}
			i++;
		}
		return resp;
	}
	
	CardService getCardService() throws RemoteException{
		
		return randomService();
	}

}
