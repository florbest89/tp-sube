package ar.edu.itba.pod.mmxivii.sube.balancer;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_CLIENT_BIND;
import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_REGISTRY_BIND;
import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_SERVICE_REGISTRY_BIND;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import javax.annotation.Nonnull;

import ar.edu.itba.pod.mmxivii.sube.common.BaseMain;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;

public class Main extends BaseMain{
	
	
	private static Main main = null;
	CardServiceRegistryImpl cardServiceRegistry;
	CardClientImpl cardClient;
	CardRegistry cardRegistry;
	private Main(@Nonnull String[] args) throws RemoteException, NotBoundException
	{
		super(args, DEFAULT_CLIENT_OPTIONS);
		getRegistry();
		setDelay();
		//aca expongo la interfaz para que se puedan registrar
		//final CardServiceRegistryImpl cardServiceRegistry = new CardServiceRegistryImpl();
		cardServiceRegistry = new CardServiceRegistryImpl();
		bindObject(CARD_SERVICE_REGISTRY_BIND, cardServiceRegistry);

		
		//busco el server y expongo la interfaz para los clientes
		//final CardRegistry cardRegistry = Utils.lookupObject(CARD_REGISTRY_BIND);
		cardRegistry = Utils.lookupObject(CARD_REGISTRY_BIND);
		//final CardClientImpl cardClient = new CardClientImpl(cardRegistry, cardServiceRegistry);
		cardClient = new CardClientImpl(cardRegistry, cardServiceRegistry);
		bindObject(CARD_CLIENT_BIND, cardClient);
	}

	public static void main(String[] args) throws Exception {
		main = new Main(args);
		main.run();
	}
		

	private void run()
	{
		System.out.println("Starting Balancer!");
		final Scanner scan = new Scanner(System.in);
		String line;
		do {
			line = scan.next();
			if(line.equals("c")){
				try {
					System.out.println("cantidad de servicios registrados: " + cardServiceRegistry.getServices().size());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Balancer running");
			
		} while(!"x".equals(line));
		shutdown();
		System.out.println("Balancer exit.");
		System.exit(0);

	}

	public static void shutdown()
	{
		main.unbindObject(CARD_SERVICE_REGISTRY_BIND);
		main.unbindObject(CARD_CLIENT_BIND);
	}
}
