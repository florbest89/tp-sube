package ar.edu.itba.pod.mmxivii.sube.balancer;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_REGISTRY_BIND;
import static ar.edu.itba.pod.mmxivii.sube.common.Utils.checkNotNull;
import static ar.edu.itba.pod.mmxivii.sube.common.Utils.delay;

import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import ar.edu.itba.pod.mmxivii.sube.common.Card;
import ar.edu.itba.pod.mmxivii.sube.common.CardClient;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.CardService;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;

public class CardClientImpl extends UnicastRemoteObject implements CardClient
{
	private static final long serialVersionUID = 3498345765116694167L;
	private CardRegistry cardRegistry;
	private final CardServiceRegistryImpl cardServiceRegistry;

	public CardClientImpl(@Nonnull CardRegistry cardRegistry, @Nonnull CardServiceRegistryImpl cardServiceRegistry) throws RemoteException
	{
		super();
		this.cardRegistry = cardRegistry;
		this.cardServiceRegistry = cardServiceRegistry;
	}

	@Nonnull
	@Override
	public Card newCard(@Nonnull String cardHolder, @Nonnull String label) throws RemoteException
	{
		Card resp = null;
		delay();
		try {
			resp = cardRegistry.newCard(cardHolder,label);
		} catch (ConnectException e) {
			try {
				System.out.println("aca tambien?");
				reconnectCardRegistry();
				return cardRegistry.newCard(cardHolder,label);
			} catch (NotBoundException e1) {
				//noinspection ConstantConditions (esto no deberia pasar, hay que cambiar esto en el contrato para avisar)
				return null; // @ToDo cambiar a algo más representativo
			}
		}
		if(resp == null){
			try {
				reconnectCardRegistry();
				return cardRegistry.newCard(cardHolder,label);
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		}
		return resp;
	}

	private void reconnectCardRegistry() throws NotBoundException
	{
		cardRegistry = Utils.lookupObject(CARD_REGISTRY_BIND);
	}

	@Nullable
	@Override
	public Card getCard(@Nonnull UID id) throws RemoteException
	{
		Card resp = null;
		delay();
		try {
			resp =  cardRegistry.getCard(checkNotNull(id));
		} catch (ConnectException e) {
			try {
				reconnectCardRegistry();
				return cardRegistry.getCard(checkNotNull(id));
			} catch (NotBoundException e1) {
				return null; // @ToDo cambiar a algo más representativo
			}
		}
		if(resp == null){
			try {
				reconnectCardRegistry();
				return cardRegistry.getCard(checkNotNull(id));
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		}
		return resp;
	}

	@Override
	public double getCardBalance(@Nonnull UID id) throws RemoteException
	{
		CardService cs = getCardService(id);
		if(cs == null){
			return CardRegistry.COMMUNICATIONS_FAILURE;
		}
		double resp = -1.0;
		try {
			resp = cs.getCardBalance(id);
		} catch (RemoteException e) {
			cardServiceRegistry.unRegisterService(cs);
			return getCardBalance(id);

		}
		return resp;
	}

	@Override
	public double travel(@Nonnull UID id, @Nonnull String description, double amount) throws RemoteException
	{		
		CardService cs = getCardService(id);
		if(cs == null){
			//no hay nodos conectados
			return CardRegistry.COMMUNICATIONS_FAILURE;
		}
		double resp = -1.0;
		try {
			resp = cs.travel(id, description, amount);
		} catch (RemoteException e) {
			cardServiceRegistry.unRegisterService(cs);
			return travel(id, description, amount);

		}
		return resp;
	}

	@Override
	public double recharge(@Nonnull UID id, @Nonnull String description, double amount) throws RemoteException
	{
		CardService cs = getCardService(checkNotNull(id));
		if(cs == null){
			return CardRegistry.COMMUNICATIONS_FAILURE;
		}

		double resp = -1.0;
		try{
			resp = cs.recharge(id, description, amount);;
		}catch (RemoteException e) { 
			cardServiceRegistry.unRegisterService(cs);
			return recharge(id, description, amount);
			//System.out.println("choteo recharge");
			//e.printStackTrace();
		}
		
		return resp;
	}

	/**
	 * Esto 
	 * @return
	 */
	private CardService getCardService(UID id)
	{
		return cardServiceRegistry.getCardService(id);
	}
}
