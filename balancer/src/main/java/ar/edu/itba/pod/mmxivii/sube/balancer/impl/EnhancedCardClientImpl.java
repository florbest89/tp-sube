package ar.edu.itba.pod.mmxivii.sube.balancer.impl;

import java.rmi.RemoteException;

import ar.edu.itba.pod.mmxivii.sube.balancer.CardClientImpl;
import ar.edu.itba.pod.mmxivii.sube.balancer.CardServiceRegistryImpl;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.CardService;
@Deprecated
public class EnhancedCardClientImpl extends CardClientImpl{

	EnhancedCardServiceRegistryImpl cardServiceRegistry;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EnhancedCardClientImpl(CardRegistry cardRegistry,
			EnhancedCardServiceRegistryImpl cardServiceRegistry) throws RemoteException {
		super(cardRegistry, cardServiceRegistry);
		this.cardServiceRegistry = cardServiceRegistry;

	}
	
	private CardService getCardService()
	{
		try {
			return cardServiceRegistry.getCardService();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

}
