package ar.edu.itba.pod.mmxivii.sube.client;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_CLIENT_BIND;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Random;

import ar.edu.itba.pod.mmxivii.sube.common.Card;
import ar.edu.itba.pod.mmxivii.sube.common.CardClient;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;

public class Client implements Runnable{
	private Card card = null;
	private CardClient cardClient = null;
	private int sleepBoundTime = 500;
	private double myBalance = 0.0;
	private int id;

	public Client(int id) {
		try {
			this.id = id;
			cardClient = Utils.lookupObject(CARD_CLIENT_BIND);
		} catch (NotBoundException e) {
			System.err.println("Could not find RMI Registry");
			//e.printStackTrace();
		}
		try {
			this.card = cardClient.newCard("alumno", "tarjeta");
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}
	
	private static String errorParse(double bondi){
		String resp = "UNKNOWN ERROR";
		if(bondi == CardRegistry.COMMUNICATIONS_FAILURE){
			resp = "COMMUNICATIONS_FAILURE";
		} else if(bondi == CardRegistry.CARD_NOT_FOUND){
			resp = "CARD_NOT_FOUND";
		} else if(bondi == CardRegistry.CANNOT_PROCESS_REQUEST){
			resp = "CANNOT_PROCESS_REQUEST";
		} else if(bondi == CardRegistry.SERVICE_TIMEOUT){
			resp = "SERVICE_TIMEOUT";
		} else if(bondi == CardRegistry.MAX_BALANCE){
			resp = "MAX_BALANCE";
		}
		return resp;
	}
		
	@Override
	public void run() {
		while(true){
			double bondi = 0;
			//System.out.print("Client: "+ id + );
			try {
				bondi = cardClient.travel(card.getId(), "bondi", 3);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
			//System.out.println("bondi = " + bondi);
			double recharge = 0;
			if(bondi == CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE){
				try {
					recharge = cardClient.recharge(card.getId(), "regarga",100);
					if(recharge < 0){
						System.out.println(errorParse(recharge));
					}else{
						myBalance +=100;
						System.out.println("Client: "+ id + "recharging = " + recharge);
						if(myBalance != recharge){
							System.out.println("Client: "+ id + "WARNING MY BALANCE DIFFER FROM SERVER: server= "+recharge + ", myBalance= "+myBalance);						}
					}
					if(recharge > 0){
						bondi = cardClient.travel(card.getId(), "bondi", 3);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			if(bondi < 0){
				System.out.println(errorParse(bondi));
			}else{
				myBalance -= 3;
				System.out.println("Client: "+ id + "bondi = " + bondi);
				if(myBalance != bondi){
					System.out.println("Client: "+ id + "WARNING MY BALANCE DIFFER FROM SERVER: server= "+bondi + ", myBalance= "+myBalance);
				}
			}
			Random r = new Random();
			try {
				Thread.sleep(r.nextInt(sleepBoundTime));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
