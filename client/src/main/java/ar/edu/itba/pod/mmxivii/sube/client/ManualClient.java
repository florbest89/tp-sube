package ar.edu.itba.pod.mmxivii.sube.client;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_CLIENT_BIND;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.Scanner;

import ar.edu.itba.pod.mmxivii.sube.common.Card;
import ar.edu.itba.pod.mmxivii.sube.common.CardClient;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;

public class ManualClient implements Runnable{
	//para que me permita pushear
	private Card card = null;
	private CardClient cardClient = null;
	private int sleepBoundTime = 1;
	private double myBalance = 0.0; 

	public ManualClient() {
		try {
			cardClient = Utils.lookupObject(CARD_CLIENT_BIND);
		} catch (NotBoundException e) {
			System.err.println("Could not find RMI Registry");
			//e.printStackTrace();
		}
		try {
			this.card = cardClient.newCard("alumno", "tarjeta");
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}
	
	private static String errorParse(double bondi){
		String resp = "UNKNOWN ERROR";
		if(bondi == CardRegistry.COMMUNICATIONS_FAILURE){
			resp = "COMMUNICATIONS_FAILURE";
		} else if(bondi == CardRegistry.CARD_NOT_FOUND){
			resp = "CARD_NOT_FOUND";
		} else if(bondi == CardRegistry.CANNOT_PROCESS_REQUEST){
			resp = "CANNOT_PROCESS_REQUEST";
		} else if(bondi == CardRegistry.SERVICE_TIMEOUT){
			resp = "SERVICE_TIMEOUT";
		} else if(bondi == CardRegistry.MAX_BALANCE){
			resp = "MAX_BALANCE";
		}
		return resp;
	}
		
	@Override
	public void run() {
		final Scanner scan = new Scanner(System.in);
		System.out.println("Manual Client");
		String line;
		double bondi = 0;
		do {
			line = scan.next();
			//
			if(line.equals("viajar")){
				try {
					bondi = cardClient.travel(card.getId(), "bondi", 3);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}else if(line.equals("cargar")){
				try {
					bondi = cardClient.recharge(card.getId(), "regarga",100);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}else if(line.equals("saldo")){
				try {
					bondi = cardClient.getCardBalance(card.getId());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}else {
				bondi = -6;
			}
			if (bondi == -6){
				System.out.println("Comando inválido, los comandos válidos son: viajar, cargar y saldo");
			}else if(bondi < 0){
				System.out.println(errorParse(bondi)+": " + bondi);
			}else{
				System.out.println("resp del servidor:" + bondi);
			}
			
		} while(!"x".equals(line));
		System.out.println("Client  exit.");
		System.exit(0);
		
	}

}
