package ar.edu.itba.pod.mmxivii.sube.client;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_CLIENT_BIND;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.Nonnull;

import ar.edu.itba.pod.mmxivii.sube.common.BaseMain;
import ar.edu.itba.pod.mmxivii.sube.common.Card;
import ar.edu.itba.pod.mmxivii.sube.common.CardClient;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;

public class Main extends BaseMain {
	private CardClient cardClient = null;
	static int CLIENTS = 4;

	private Main(@Nonnull String[] args) throws NotBoundException {
		super(args, DEFAULT_CLIENT_OPTIONS);
		getRegistry();
		cardClient = Utils.lookupObject(CARD_CLIENT_BIND);
	}

	public static void main(@Nonnull String[] args) throws Exception {
		boolean auto = false;
		for (String s : args) {
			if (s.startsWith("auto")) {
				auto = true;
			} else if (s.startsWith("manual")) {
				auto = false;
			}
		}
		final Main main = new Main(args);
		// main.proRun();

		// auto = false;
		if (auto) {
			for (int i = 0; i < CLIENTS; i++)
				main.run2( i);
		} else {
			main.manualClient();
		}

	}

	private void manualClient() {
		Thread client = new Thread(new ManualClient());
		client.start();
	}

	private void run2(int i) throws RemoteException {
		Thread client = new Thread(new Client(i));
		client.start();
	}

	@SuppressWarnings("unused")
	private void run() throws RemoteException {
		System.out.println("Main.run");
		final Card card = cardClient.newCard("alumno", "tarjeta");
		final double primero = cardClient
				.recharge(card.getId(), "primero", 100);
		System.out.println("primero = " + primero);
		final double bondi = cardClient.travel(card.getId(), "bondi", 3);
		System.out.println("bondi = " + bondi);
		// cardClient.newCard()
	}

	@SuppressWarnings("unused")
	private void proRun() throws RemoteException {
		System.out.println("Main.run");
		final int cantidad = (new Random()).nextInt(1000);

		final List<Card> cards = new CopyOnWriteArrayList<Card>();
		Thread t = new Thread() {
			public void run() {
				for (int i = 0; i < cantidad; i++) {
					Card card = null;
					try {
						card = cardClient.newCard("alumno" + i, "tarjeta");
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					cards.add(card);
				}
			}
		};
		t.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (true) {
			if (Math.random() < 0.1D) {
				Card card = cards.get(new Random().nextInt(cards.size()));
				double primero = cardClient.recharge(card.getId(), "regarga",
						100);
				System.out.println("regarga = " + primero);
			} else {
				Card card = cards.get(new Random().nextInt(cards.size()));
				double bondi = cardClient.travel(card.getId(), "bondi", 3);
				System.out.println("bondi = " + bondi);

			}
		}

		// cardClient.newCard()
	}
}
