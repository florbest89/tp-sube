package ar.edu.itba.pod.mmxivii.sube.service;

import ar.edu.itba.pod.mmxivii.sube.common.Card;
import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.CardService;

import javax.annotation.Nonnull;

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.blocks.MethodCall;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.ResponseMode;
import org.jgroups.blocks.RpcDispatcher;
import org.jgroups.util.RspList;

import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class CardServiceImpl extends ReceiverAdapter implements CardService {
	private static final long serialVersionUID = 2919260533266908792L;
	private final int UPDATE_RATE = 2 * 60 * 1000;
	@Nonnull
	private final CardRegistry cardRegistry;

	private Map<UID, Double> balances;
	private Map<UID, Address> primaryCopyOwners;
	private Timer timer;
	private boolean updatingScheduled = false;

	private JChannel channel;
	private String props;
	private RpcDispatcher dispatcher;
	private MethodCall replicateCall;
	private MethodCall getPrimaryCall;
	private MethodCall getAllBalancesCall;
	private MethodCall getAllPrimaryCopyOwnersCall;
	private RequestOptions replicateOpts;
	private RequestOptions getPrimaryOpts;
	private RequestOptions getAllBalancesOpts;
	private RequestOptions getAllPrimaryCopyOwnersOpts;
	/* the node is used only for scheduleServerUpdating */
	private Node node;

	public CardServiceImpl(@Nonnull CardRegistry cardRegistry)
			throws RemoteException {
		UnicastRemoteObject.exportObject(this, 0);
		try {
			// don't know
			this.channel = new JChannel(props);
			this.replicateCall = new MethodCall(getClass().getMethod(
					"replicate", Address.class, CardBalance.class));
			this.getPrimaryCall = new MethodCall(getClass().getMethod(
					"getPrimaryCopy", Address.class, UID.class));
			this.getAllBalancesCall = new MethodCall(getClass().getMethod(
					"getAllBalances"));
			this.getAllPrimaryCopyOwnersCall = new MethodCall(getClass()
					.getMethod("getAllPrimaryCopyOwners"));
			this.replicateOpts = new RequestOptions(ResponseMode.GET_ALL, 5000);
			this.getPrimaryOpts = new RequestOptions(ResponseMode.GET_ALL, 5000);
			this.getAllBalancesOpts = new RequestOptions(
					ResponseMode.GET_FIRST, 5000);
			this.getAllPrimaryCopyOwnersOpts = new RequestOptions(
					ResponseMode.GET_FIRST, 5000);
			this.dispatcher = new RpcDispatcher(this.channel, this);
			this.channel.connect("singleCluster");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.cardRegistry = cardRegistry;
		this.balances = new ConcurrentHashMap<UID, Double>();
		this.primaryCopyOwners = new ConcurrentHashMap<UID, Address>();
		this.timer = new Timer();
		this.node = new Node();
		this.node.setReceiver(new NodeReceiver(this));
		this.channel.setReceiver(this);
		getAllBalancesCall.setArgs();
		// this calls are synchronous calls
		try {
			RspList response = dispatcher.callRemoteMethods(channel.getView()
					.getMembers().subList(0, 1), getAllBalancesCall,
					getAllBalancesOpts);
			this.balances = (Map<UID, Double>) response.getFirst();
			System.out.println(balances.values());
		} catch (Exception e) {
			e.printStackTrace();
		}
		getAllPrimaryCopyOwnersCall.setArgs();
		// this calls are synchronous calls
		try {
			RspList response = dispatcher.callRemoteMethods(channel.getView()
					.getMembers().subList(0, 1), getAllPrimaryCopyOwnersCall,
					getAllPrimaryCopyOwnersOpts);
			this.primaryCopyOwners = (Map<UID, Address>) response.getFirst();
		} catch (Exception e) {
			e.printStackTrace();
		}
		node.connectTo("coordinatorCluster");
		scheduleServerUpdating();
	}

	@Override
	public double getCardBalance(@Nonnull UID id) throws RemoteException {
		if (!balances.containsKey(id)) {
			balances.put(id, cardRegistry.getCardBalance(id));
		}
		return balances.get(id);
	}

	@Override
	public double travel(@Nonnull UID id, @Nonnull String description,
			double amount) throws RemoteException {
		if (!balances.containsKey(id)) {
			balances.put(id, cardRegistry.getCardBalance(id));
		}
		obtainPrimaryCopy(id);
		if (balances.get(id) < amount) {
			return CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE;
		}
		if (amount > 100 || amount < 1)
			return CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE;
		Double result = balances.get(id) - amount;
		balances.put(id, result);
		replicateToOthers(id);
		return result;
	}

	@Override
	public double recharge(@Nonnull UID id, @Nonnull String description,
			double amount) throws RemoteException {
		if (!balances.containsKey(id)) {
			balances.put(id, cardRegistry.getCardBalance(id));
		}
		if (amount > 100 || amount < 1)
			return CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE;
		obtainPrimaryCopy(id);
		Double result = balances.get(id) + amount;
		balances.put(id, result);
		replicateToOthers(id);
		return result;
	}

	/*
	 * hace un broadcast a jgroups para replicar la info en todos los nodos
	 */
	@Deprecated
	private void replicateToOthers(UID id) {
		CardBalance cardBalance = new CardBalance(balances.get(id), id,0);
		replicateCall.setArgs(channel.getAddress(), cardBalance);
		// this calls are synchronous calls
		try {
			dispatcher.callRemoteMethods(null, replicateCall, replicateOpts);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// node.send(new OpMessage(Operations.UPDATE, cardBalance));
	}

	/*
	 * Este es el método que van a llamar desde otros nodos para replicar un
	 * valor en este nodo
	 */
	public void replicate(Address address, CardBalance cardBalance) {
		if (!address.equals(this.channel.getAddress())) {
			balances.put(cardBalance.getCard_id(), cardBalance.getCredit());
			primaryCopyOwners.put(cardBalance.getCard_id(), address);
			System.out.println(cardBalance);
		}
	}

	/*
	 * Este es el método que van a llamar desde otros nodos para obtener la
	 * copia primaria
	 */
	@Deprecated

	public CardBalance getPrimaryCopy(Address address, UID id) {
		if (!primaryCopyOwners.get(id).equals(channel.getAddress())) {
			try {
				getPrimaryCall.setArgs(channel.getAddress(), id);
				CardBalance balance = (CardBalance) dispatcher
						.callRemoteMethod(primaryCopyOwners.get(id),
								getPrimaryCall, getPrimaryOpts);
				balances.put(id, balance.getCredit());
			} catch (Exception e) {
				/* Nothing to do in this case */
			}
		}
		primaryCopyOwners.put(id, address);
		return new CardBalance(balances.get(id), id,0);
	}

	public Map<UID, Double> getAllBalances() {
		return balances;
	}

	public Map<UID, Address> getAllPrimaryCopyOwners() {
		return primaryCopyOwners;
	}

	private void obtainPrimaryCopy(UID id) {
		if (primaryCopyOwners.containsKey(id)
				&& !primaryCopyOwners.get(id).equals(channel.getAddress())) {
			try {
				getPrimaryCall.setArgs(channel.getAddress(), id);
				CardBalance balance = (CardBalance) dispatcher
						.callRemoteMethod(primaryCopyOwners.get(id),
								getPrimaryCall, getPrimaryOpts);
				balances.put(id, balance.getCredit());
			} catch (Exception e) {
				/* The primary copy owner is dead, so I should take ownership */
				System.out.println("The owner is dead????");
			}
			System.out.println("now I'm the owner!!! muajajaja");
			primaryCopyOwners.put(id, channel.getAddress());
		}
	}

	public void updateBalance(CardBalance cb) {
		balances.put(cb.getCard_id(), cb.getCredit());
	}

	/*
	 * Schedules this coordinator node to update the main server with the
	 * specified update rate. To avoid blocking the node, the idea is to start a
	 * background thread that updates the server (runs only once). Since the
	 * main server has a delay of 0.2-1 sec per addCardOperation, and per
	 * getCardBalance, --> in the worst case scenario it takes 2 sec to update
	 * one card. To meet the requirement that an operation on a card must be
	 * registered in the server within 2 minutes, the service cluster can handle
	 * at most 60 cards = 120 sec = 2 min.
	 */
	protected void scheduleServerUpdating() {
		if (isCoord()) {
			if (!updatingScheduled) {
				System.out.println("server updating");
				timer.scheduleAtFixedRate(new TimerTask() {
					public void run() {
						new UpdateServerThread(balances.entrySet(),
								cardRegistry).start();
					}
				}, UPDATE_RATE, UPDATE_RATE);
				updatingScheduled = true;
			}
		}
	}

	private boolean isCoord() {
		return channel.getView().getMembers().get(0)
				.equals(channel.getAddress());
	}

	// Cancels the scheduled updating of the server.
	protected void stopServerUpdating() {
		timer.cancel();
		updatingScheduled = false;
	}

	/*
	 * New view --> change in member list --> node might have crashed --> could
	 * be the coordinator --> necessary to reschedule the updating of the
	 * server.
	 */
	public void viewAccepted(View view) {
		scheduleServerUpdating();
	}
}
