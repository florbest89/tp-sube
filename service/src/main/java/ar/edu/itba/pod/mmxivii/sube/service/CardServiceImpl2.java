package ar.edu.itba.pod.mmxivii.sube.service;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Nonnull;

import org.apache.commons.codec.language.bm.Languages.SomeLanguages;
import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.CardService;

public class CardServiceImpl2 extends ReceiverAdapter implements CardService,
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2919260533266908792L;
	private final int UPDATE_RATE = 1 * 60 * 1000;
	private Timer timer;
	private boolean updatingScheduled = false;
	private final CardRegistry cardRegistry;
	ConcurrentHashMap<UID, Double> balances;
	ConcurrentHashMap<UID, Long> op_count;
	Set<UID> primaryCopies = new CopyOnWriteArraySet<UID>();
	// locks until new node is sync
	AtomicBoolean lock = new AtomicBoolean(false);
	BlockingQueue<UID> unknowCards;
	JChannel channel;

	AtomicBoolean askingForCard = new AtomicBoolean(false);
	AtomicBoolean askingForPrimaryCopy = new AtomicBoolean(false);
	AtomicBoolean primaryCopyUpdate = new AtomicBoolean(false);

	// AtomicBoolean busy = new AtomicBoolean(false);

	public CardServiceImpl2(CardRegistry cardRegistry) throws RemoteException {
		UnicastRemoteObject.exportObject(this, 0);

		this.balances = new ConcurrentHashMap<UID, Double>();
		this.op_count = new ConcurrentHashMap<UID, Long>();
		unknowCards = new LinkedBlockingDeque<UID>();
		this.cardRegistry = cardRegistry;
		this.timer = new Timer();
		try {
			channel = new JChannel();
			channel.setReceiver(this);
			channel.connect("my_pod_cluster");
		} catch (Exception e) {
			System.err.println("Failed to create a cluster");
			// e.printStackTrace();
		}
		// syncronizeData();
	}

	// private void syncronizeData() {
	//
	// }

	/*
	 * private void busy() { while (busy.get()) { ; }
	 * 
	 * }
	 */

	@Override
	public double getCardBalance(@Nonnull UID id) throws RemoteException {
		// TODO:
		// Ask from cluster fisrt
		if (!balances.containsKey(id)) {
			balances.put(id, cardRegistry.getCardBalance(id));
			op_count.put(id, 0L);
		}
		return balances.get(id);
	}

	@Override
	public double recharge(@Nonnull UID id, @Nonnull String description,
			double amount) throws RemoteException {
		double code = askForPrimaryCopyOwnership(id);
		if(code < 0){
			return code;
		}
		if (amount > 100 || amount < 1)
			return CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE;
		// aca hacer lo de 0.555
		Double result = balances.get(id) + amount;

		// double resp = cardRegistry.addCardOperation(id, description, amount);
		op_count.put(id, op_count.get(id) + 1);
		balances.put(id, result);
		replicateToOthers(id);
		return result;
	}

	@Override
	public double travel(@Nonnull UID id, @Nonnull String description,
			double amount) throws RemoteException {
		double code = askForPrimaryCopyOwnership(id); // bloqueante
		if(code < 0){
			return code;
		}
		if (balances.get(id) < amount) {
			return CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE;
		}
		if (amount > 100 || amount < 1)
			return CardRegistry.OPERATION_NOT_PERMITTED_BY_BALANCE;
		Double result = balances.get(id) - amount;
		op_count.put(id, op_count.get(id) + 1);
		balances.put(id, result);
		replicateToOthers(id);
		return result;

	}

	ConcurrentHashMap<Address, Boolean> primarycopy_response;
	ConcurrentHashMap<Address, CardBalance> primarycopy_response_data;
	ConcurrentHashMap<Address, Boolean> primarycopy_response_unkowncards;
	private AtomicBoolean clusterChangeWhileAskingPrimaryCopy = new AtomicBoolean(
			false);

	private double askForPrimaryCopyOwnership(UID id) {
		/*
		 * Me tengo que bloquear hasta que se resuelva 1er caso pueden pasar 2
		 * cosas: 1.a) no la conozco a la tarjeta: estoy solo en el cluster la
		 * pido al server y me pongo la primary copy 1.b) la conozco a la
		 * tarjeta estoy solo en el cluster, me quedo con la copia que conzco.
		 * 2do caso No estoy solo en el cluster. Pido la transferencia de la
		 * copia primaria. pueden pasar 3 cosas: 2.a) El que la tiene, me la
		 * pasa, sigo procesando el request, la nueva primary copy soy yo. 2.b)
		 * Nadie la tiene (Me responden que no la tienen: me mandan su copia):
		 * 2.b.i) Si todos responden que no la tienen, me quedo con las que
		 * mandaron, la de count mas alto. La nueva primary copy soy yo 2.b.ii)
		 * Si ninguno la conoce: la pido al server, la nueva primary copy soy
		 * yo. 2.c) Nadie responde: Ver que hacer! o Falta que uno o mas
		 * respondan! VER QUE HACER
		 * 
		 * Condiciones de desbloqueo: >Estoy solo en el cluster, la pido al
		 * cluster y me desbloqueo >Todos los miembros del cluster me
		 * respondieron que no tienen la PrimaryCopy (Actualizar con ultima
		 * copia) y me desbloqueo >Todos los miembros del cluster me
		 * respondieron que no la conocen, la pido al server y me desbloqueo
		 * >Timeout (Nadie respondio) (????)
		 */
		if (primaryCopies.contains(id)) {
			return 1;
		}
		askingForPrimaryCopy.set(true);

		if (channel.getView().size() == 1) {
			if (balances.containsKey(id)) {
				// si soy el unico y tengo la info, me quedo con la info que
				// tengo
				primaryCopies.add(id);
				return 1;
			}
			try {
				balances.put(id, cardRegistry.getCardBalance(id));
				op_count.put(id, 0L);
				primaryCopies.add(id);
				System.out.println("asking card to server");
				askingForPrimaryCopy.set(false);

			} catch (RemoteException e) {
				/*
				 * TODO: Ver que hacer en este caso, la idea es retornar un
				 * error al cliente
				 */
				e.printStackTrace();
				System.err.println("servidor caido");
				return CardRegistry.CANNOT_PROCESS_REQUEST;
			}
			return 1;
		}
		System.out.println("asking primary copy of:" + id
				+ "to cluster members");
		primarycopy_response = new ConcurrentHashMap<Address, Boolean>(channel
				.getView().getMembers().size());
		primarycopy_response_data = new ConcurrentHashMap<Address, CardBalance>(
				channel.getView().getMembers().size());
		primarycopy_response_unkowncards = new ConcurrentHashMap<Address, Boolean>(
				channel.getView().getMembers().size());
		List<Address> members = new CopyOnWriteArrayList<Address>(channel
				.getView().getMembers());
		for (Address member : members) {
			if (!member.equals(channel.getAddress())) {
				primarycopy_response.put(member, false);
				primarycopy_response_data.put(member, new CardBalance(0,
						new UID(), -1));
				primarycopy_response_unkowncards.put(member, false);

			}

			sendOpMessage(member,
					new OpMessage(Operations.ASK_PRIMARY_COPY, id));
		}
		boolean asktoserver = false;
		boolean mayorcountcopy = false;

		while (askingForPrimaryCopy.get()) {
			// si alguno me mando la primary copy me desbloqueo al recibir
			// mensaje

			// si todos me responden que no la conocen, me proclamo owner, y la
			// pido al server
			boolean unknownCardsFlag = true;
			boolean knownCardFlag = true;
			if(primarycopy_response!= null){
				for (Address member : primarycopy_response.keySet()) {
					/*
					 * si el miembro respondio me fijo que respondio: >si unknown =
					 * true respondio que no lo conoce >si unknown = false a lo
					 * mejor respondio la data.
					 */
					synchronized (mutex) {
						if(primarycopy_response != null && primarycopy_response_unkowncards != null && primarycopy_response_data!=null){
							if (primarycopy_response.containsKey(member)
									&& primarycopy_response_unkowncards.containsKey((member))) {
								unknownCardsFlag &= (primarycopy_response.get(member) && primarycopy_response_unkowncards
										.get(member));
								knownCardFlag &= (primarycopy_response.get(member));
							}
							if( primarycopy_response_data.containsKey(member)){
									if (primarycopy_response_data.get(member).count() > -1) {
										knownCardFlag &= true;
									} else {
										knownCardFlag &= false;
									}
							}
						}
	
					}
				}
			}
			if (clusterChangeWhileAskingPrimaryCopy.get()) {
				askingForPrimaryCopy.set(false);
				mayorcountcopy = true;
				clusterChangeWhileAskingPrimaryCopy.set(false);
			}
			if (unknownCardsFlag && !knownCardFlag) {
				asktoserver = true;
				askingForPrimaryCopy.set(false);
				System.out.println("me desbloqueo porque me mandaron que ninguno la conoce");
			}
			if (!unknownCardsFlag && knownCardFlag) {
				// quedarme con la mas alta.
				mayorcountcopy = true;
				askingForPrimaryCopy.set(false);
				System.out.println("me desbloqueo porque me mandaron que todos la conocen y pero no la primary copy");

			}
			if ((unknownCardsFlag && knownCardFlag)) {
				System.out.println("no puede pasar");
				mayorcountcopy = true;
				askingForPrimaryCopy.set(false);
			}

			// TODO: Me manda su copia (el primary copy owner se cayo)
			/*
			 * boolean flag = true; for(Address member:
			 * primarycopy_response.keySet()){ flag &=
			 * primarycopy_response.get(member); } if(flag){
			 * askingForPrimaryCopy.set(false); }
			 */

		}

		if (asktoserver) {
			System.out.println("nadie conocia" + id + " la pido al servidor");
			try {
				balances.put(id, cardRegistry.getCardBalance(id));
				op_count.put(id, 0L);
				primaryCopies.add(id);
				System.out.println("asking card to server");
				askingForPrimaryCopy.set(false);

			} catch (RemoteException e) {
				/*
				 * TODO: Ver que hacer en este caso, la idea es retornar un
				 * error al cliente
				 */
				e.printStackTrace();
				System.err.println("servidor caido");
				return CardRegistry.CANNOT_PROCESS_REQUEST;
			}
		}

		CardBalance maxOpsCardBalance = null;

		if (mayorcountcopy) {
			if (!balances.containsKey(id)) {
				System.out.println("no la conozco");
				maxOpsCardBalance = new CardBalance(0, new UID(), -1);

			} else {
				System.out.println("la conozco");
				maxOpsCardBalance = new CardBalance(balances.get(id), id,
						op_count.get(id));
			}
			System.out.println("veo entre las que llegaron");
			for (Address member : members) {
				if (!member.equals(channel.getAddress())) {
					if(primarycopy_response_data != null){
						if(primarycopy_response_data.containsKey(member)){
							CardBalance cb = primarycopy_response_data.get(member);
							if (maxOpsCardBalance.count() > cb.count()) {
								maxOpsCardBalance = cb;
							}
						}
					}
				}
			}
			balances.put(maxOpsCardBalance.getCard_id(),
					maxOpsCardBalance.getCredit());
			op_count.put(maxOpsCardBalance.getCard_id(),
					maxOpsCardBalance.count());
			primaryCopies.add(maxOpsCardBalance.getCard_id());
		}

		primarycopy_response = null;
		primarycopy_response_data = null;
		primarycopy_response_unkowncards = null;
		
		return 1;

	}

	private Object mutex = new Object();

	public void receive(Message msg) {
		if (msg == null || msg.getSrc() == null) {
			return;
		}
		if (msg.getSrc().equals(channel.getAddress())) {
			return;
		}
		if (!(msg.getObject() instanceof OpMessage)) {
			System.out.println("basura descartar");
			return;
		}
		// System.out.println("esto debe ser el nuevo saldo u otra operacion");
		OpMessage om = (OpMessage) msg.getObject();
		if (om.getOperation() == Operations.UPDATE) {
			CardBalance cb = (CardBalance) om.getData();
			this.updateBalance(cb);
		} else if (om.getOperation() == Operations.ASK_PRIMARY_COPY) {
			UID uid = (UID) om.getData();
			if (primaryCopies.contains(uid)) {
				System.out.println("someone asked primary copy and i'm owner");
				sendOpMessage(msg.getSrc(), new OpMessage(
						Operations.ASK_PRIMARYCOPY_OWNER, new CardBalance(
								balances.get(uid), uid, op_count.get(uid))));
				primaryCopies.remove(uid); // ya no tengo la primary copy
			} else {
				// le respondo lo que conozco
				if (balances.containsKey(uid)) {
					System.out
							.println("someone asked primary copy and i'm NOT the owner");
					CardBalance cb = new CardBalance(balances.get(uid), uid,
							op_count.get(uid));
					sendOpMessage(msg.getSrc(), new OpMessage(
							Operations.ASK_PRIMARYCOPY_NOT_OWNER, cb));
					System.out.println("envio: " + cb);
				} else {
					System.out
							.println("someone asked primary copy and i'm NOT the owner and DONT KNOW CARD");
					sendOpMessage(msg.getSrc(), new OpMessage(
							Operations.ASK_PRIMARYCOPY_UNKNOWNCARD,
							new CardBalance(0, new UID(), 0)));
				}
			}
		} else if (om.getOperation() == Operations.ASK_PRIMARYCOPY_OWNER) {
			// me mandaron la copia primaria
			CardBalance cb = (CardBalance) om.getData();
			System.out.println("recived primary copy of:" + cb.getCard_id()
					+ "-" + cb.getCredit() + "cant-" + cb.count());
			primaryCopies.add(cb.getCard_id());
			Address source = msg.getSrc();
			primaryCopies.add(cb.getCard_id());
			balances.put(cb.getCard_id(), cb.getCredit());
			op_count.put(cb.getCard_id(), cb.count());
			askingForPrimaryCopy.set(false);
			// primarycopy_response.put(source, true);
			// primarycopy_response_data.put(source, cb);

		} else if (om.getOperation() == Operations.ASK_PRIMARYCOPY_NOT_OWNER) {
			// me respondieron que no tiene la copia primaria
			CardBalance cb = (CardBalance) om.getData();
			Address source = msg.getSrc();
			synchronized (mutex) {
				if(primarycopy_response!=null){
					if (primarycopy_response.containsKey(source) && primarycopy_response_unkowncards.containsKey((source))) {
						primarycopy_response.put(source, true);
						primarycopy_response_data.put(source, cb);
					}
				}
			}

			// primarycopy_response_data.put(source, cb);
		} else if (om.getOperation() == Operations.ASK_PRIMARYCOPY_UNKNOWNCARD) {
			// me respondieron que no conocen esa copia primaria
			Address source = msg.getSrc();
			synchronized (mutex) {
				primarycopy_response.put(source, true);
				primarycopy_response_unkowncards.put(source, true);
			}

		}
		super.receive(msg);
	}

	private void sendOpMessage(Address destinationAddress, OpMessage opMessage) {
		Message msg = new Message(destinationAddress, null, opMessage);
		try {
			channel.send(msg);
		} catch (Exception e) {
			System.out.println("Failed when broadcasting message:" + opMessage
					+ "to: " + destinationAddress);
			// e.printStackTrace();
		}
	}

	protected void scheduleServerUpdating() {
		if (isCoord()) {
			if (!updatingScheduled) {
				System.out.println("server updating");
				try {
					TimerTask tt = new TimerTask() {
						public void run() {
							new UpdateServerThread(balances.entrySet(),
									cardRegistry).start();
						}
					};
					timer.scheduleAtFixedRate(tt, UPDATE_RATE, UPDATE_RATE);
				} catch (Exception e) {
					e.printStackTrace();
				}
				updatingScheduled = true;
			}
		}
	}

	private boolean isCoord() {
		return channel.getView().getMembers().get(0)
				.equals(channel.getAddress());
	}

	// Cancels the scheduled updating of the server.
	protected void stopServerUpdating() {
		timer.cancel();
		updatingScheduled = false;
	}

	@Override
	public void viewAccepted(View view) {
		// lock.set(true);
		System.out.println(view);
		if (askingForPrimaryCopy.get()) {
			System.out.println("i was waiting a primary copy and cluster changed");
			clusterChangeWhileAskingPrimaryCopy.set(true);
		}
		scheduleServerUpdating();
		super.viewAccepted(view);
	}

	/*
	 * Do this only if i'm the owner of Primary Copy
	 */
	private void replicateToOthers(UID id) {
		CardBalance cardBalance = new CardBalance(balances.get(id), id,
				op_count.get(id));
		broadcastOpMessage(new OpMessage(Operations.UPDATE, cardBalance));
	}

	public void updateBalance(CardBalance cb) {
		// solo si no tengo la primary copy
		if (!primaryCopies.contains(cb.getCard_id())) {
			// System.out.println("updating: " + cb);
			balances.put(cb.getCard_id(), cb.getCredit());
			op_count.put(cb.getCard_id(), cb.count());
		} /*else {
			if (op_count.get(cb.getCard_id()) < cb.count()){
				System.out.println("adjusting balance");
				balances.put(cb.getCard_id(), cb.getCredit());
			}
		}*/

	}

	public void updatePKBalance(CardBalance cb) {
		balances.put(cb.getCard_id(), cb.getCredit());
		op_count.put(cb.getCard_id(), cb.count());
		primaryCopies.add(cb.getCard_id());
	}

	public void broadcastOpMessage(OpMessage opMessage) {

		Message msg = new Message(null, null, opMessage);
		try {
			channel.send(msg);
		} catch (Exception e) {
			System.out.println("Failed when broadcasting message:" + opMessage);
			// e.printStackTrace();
		}
	}
}
