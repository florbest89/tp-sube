package ar.edu.itba.pod.mmxivii.sube.service;

import java.rmi.server.UID;

public class CardBalance implements Command {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double credit;
	private UID card_id;
	private long count;

	
	public CardBalance(double credit, UID card_id, long count) {
		super();
		this.credit = credit;
		this.card_id = card_id;
		this.count = count;
	}
	public double getCredit() {
		return credit;
	}

	public UID getCard_id() {
		return card_id;
	}
	
	public long count() {
		return count;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return card_id + "-> " + credit+ "count:" + count;
	}


}
