package ar.edu.itba.pod.mmxivii.sube.service;

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.Receiver;

	

public class Node {
	
	private JChannel channel;
	
	public Node(){
		
		try {
			this.channel = new JChannel();
		} catch (Exception e) {
			System.out.println("No se ha podido crear el canal");
			e.printStackTrace();
		}		
		
	}
	
	public void connectTo(String clusterName){
	
		try {
			channel.connect(clusterName);
		} catch (Exception e) {
			System.out.println("No se ha podido conectar al grupo.");
			System.out.println(channel);
			e.printStackTrace();
		}
		
	}
	
	public void disconnect(){
		channel.disconnect();
		channel.close();
	}
	
	public void send(Object message){
		sendTo(message,null);
	}
	
	public void sendTo(Object message, Address addr){
		
		Message msg;
		
		if(addr == null){
			msg = new Message();
		}else{
			msg = new Message(addr);
		}
		
		try {
			channel.send(msg.setObject(message));
		} catch (Exception e) {
			System.out.println("No se ha podido enviar el mensaje");
			e.printStackTrace();
		}
		
	}
	
	public void setReceiver(Receiver rec){
		channel.setReceiver(rec);
	}
	
	public boolean isCoord(){
		return channel.getView().getMembers().get(0).equals(channel.getAddress());
	}
}
