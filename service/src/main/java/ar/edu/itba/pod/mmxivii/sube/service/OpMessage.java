package ar.edu.itba.pod.mmxivii.sube.service;

import java.io.Serializable;
import java.rmi.server.UID;

public class OpMessage implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Operations operation;
	private Serializable s;
	
	
	
	public OpMessage(Operations operation, Serializable serializable){
		this.operation = operation;
		//this.amount = amount;
		this.s = serializable;
		
	}


	public Operations getOperation() {
		return operation;
	}


	public void setOperation(Operations operation) {
		this.operation = operation;
	}


	public Serializable getData() {
		return s;
	}


	public void setData(Command object) {
		this.s = object;
	}
	
	
	

}
