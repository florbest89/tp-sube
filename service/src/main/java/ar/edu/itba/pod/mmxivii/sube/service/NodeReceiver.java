package ar.edu.itba.pod.mmxivii.sube.service;

import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;


public class NodeReceiver extends ReceiverAdapter{
	
	CardServiceImpl cardService;
	
	public NodeReceiver(CardServiceImpl cs) {
		this.cardService = cs;
	}
	@Override
	public void receive(Message msg) {
		super.receive(msg);
	}
	
	/* New view --> change in member list --> node might have crashed --> 
	 * could be the coordinator --> necessary to reschedule the updating of the server. 
	 */
	public void viewAccepted(View view){
		cardService.scheduleServerUpdating();
	}
}
