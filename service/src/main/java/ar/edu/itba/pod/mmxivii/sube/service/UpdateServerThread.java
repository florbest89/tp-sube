package ar.edu.itba.pod.mmxivii.sube.service;

import static ar.edu.itba.pod.mmxivii.sube.common.Utils.CARD_REGISTRY_BIND;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UID;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ar.edu.itba.pod.mmxivii.sube.common.CardRegistry;
import ar.edu.itba.pod.mmxivii.sube.common.Utils;

public class UpdateServerThread extends Thread {
	private Set<Map.Entry<UID,Double>> set;
	private CardRegistry cardRegistrya;
    UpdateServerThread(Set<Map.Entry<UID,Double>> set, CardRegistry cardRegistry) {
        this.set = set;
        this.cardRegistrya = cardRegistry;
    }

    public void run() {
    	UID id;
    	double cacheBal;
    	double serverBal;
    	//Change description!
    	//..
    	String description = "description";
    	CardRegistry cardRegistry = null;
		try {
			cardRegistry = Utils.lookupObject(CARD_REGISTRY_BIND);
		} catch (NotBoundException e2) {
			e2.printStackTrace();
		}
		for (Entry<UID, Double> entry : set) {
		    id = entry.getKey();
		    cacheBal = entry.getValue();
		    /* Later maybe change so that we save the old value from the server 
		     * so we don't have to fetch it every time (--> delay!)
		     */
		    try {
				serverBal = cardRegistry.getCardBalance(id);
				if(serverBal != cacheBal){
					cardRegistry.addCardOperation(id, description, (cacheBal - serverBal));
				}
	    	} catch (RemoteException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
	    		try {
					reconnectCardRegistry();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
    }
    
    private void reconnectCardRegistry() throws NotBoundException
	{
		cardRegistrya = Utils.lookupObject(CARD_REGISTRY_BIND);
	}
}

