package ar.edu.itba.pod.mmxivii.sube.service;

public enum Operations {

	UPDATE , ASK_PRIMARY_COPY, ASK_PRIMARYCOPY_OWNER, ASK_PRIMARYCOPY_NOT_OWNER, ASK_PRIMARYCOPY_UNKNOWNCARD;
	
}
