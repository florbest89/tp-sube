package org.sube.rmi.registry;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws RemoteException
    {
    	/*
    	 * TODO: DESCABLEAR
    	 */
    	int port = 7242;
		Registry registry = LocateRegistry.createRegistry(port);
		System.out.println("Starting RMI registry on port: "+ port + " !");
		final Scanner scan = new Scanner(System.in);
		String line;
		do {
			line = scan.next();
		} while(!"x".equals(line));
		System.out.println("RMI Registry close.");
		System.exit(0);
    }
}
